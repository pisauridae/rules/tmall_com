/**
 * parse page in handle
 * @param page
 * @param spliderCtx
 * @constructor
 */
function MainDownload( page , pageAction) {
    var sreq = page.getRequest()

    var url = sreq.getUrl();

    // --- call invoke url ------
    pageAction.loadUrl(url);
    pageAction.sleep(3, "s");
    pageAction.waitingForElem("html");

}

/**
 * validate html after download url
 * @param downloadreq
 * @param page
 * @constructor
 */
function ValidateData(page) {
    return true

}

/**
 * define prelinke download
 * @constructor
 */
function PreDownload(downloadreq) {

    var obj = {
        url :""
    }

}
