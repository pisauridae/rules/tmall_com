
/**
 * define pre download page
 * @param page
 * @returns {boolean}
 * @constructor
 */
function  PreDownload(page) {
    console.log("hello");

    // --- flag for continue
    return false;
};


/**
 * parse page in handle
 * @param page
 * @param spliderCtx
 * @constructor
 */
function MainDownload(page ,pageAction) {

    var sreq = page.getRequest()

    var url = sreq.getUrl();

    // --- call invoke url ------
    pageAction.loadUrl(url);
    pageAction.sleep(4, "s");
    pageAction.waitingForElem("b.J_EbrandLogo");
    pageAction.waitingForElem("html");

    // --- check the element ---
    //PageAction.execJS("console.log(1234);");
    // div.sufei-dialog


}

/**
 * validate html after download url
 * @param downloadreq
 * @param page
 * @constructor
 */
function ValidateData(downloadreq, page) {
    return true

}
