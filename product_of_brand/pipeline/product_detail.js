var MongoClient = require("mongodb").MongoClient;
var Utils = require("utils").Utils;



/**
 * parse page in handle
 * @param page
 * @constructor
 */
function MainPipeline(page) {


    url = 'mongodb://localhost:27017';

    var client = new MongoClient(url);

    client.connect(  function (err) {

        var db = client.db("splider-data");

        var coll = db.collection("tasks");


        // ---- find task collection to end ----

        // --- receive data  ----
        var taskExsited = {};
        coll.findOne({
            "taskcode":"taskunit"
        }, function(err , item) {

            if (item  != undefined) {
                taskExsited.inst_id = item.inst_id;
                taskExsited.taskcode = item.taskcode;
            }
        });

        if (taskExsited.inst_id == undefined) {
            var datetime = new Date();
            // --- create and new task ----
            var idGenerator = Utils.getIdGenerator(1);
            var newTaskId = idGenerator.GenId();

            coll.insertOne({
                "inst_id":  newTaskId,
                "taskcode": "taskunit",
                "startTime": datetime.format("yyyy-M-d h:m:s.S")
            },function(err, r){

                console.log("call back insert on ");

            })
        }

        // ---- get product item  ---
        var prodInfo = page.getResultItem("productInfo");
        var tags = page.getResultItem("productCate");
        var productAttr = page.getResultItem("productAttr");
        var productGoodStatus = page.getResultItem("productGoodStatus");
        var prodPrices = page.getResultItem("prodPrices");
        var productImgs = page.getResultItem("productImgs");


        // --- create product uuid ---
        var idGenerator = Utils.getIdGenerator(1);
        var newId = idGenerator.GenId();


        // --- create product info ---
        prodInfo.id = newId;
        prodInfo.taskId = taskExsited.inst_id;

        tags.id = newId;
        productAttr.prodId = newId;

        productGoodStatus.prodId = newId;
        prodPrices.prodId = newId;
        productImgs.prodId = newId;

        // --- new add the download time ---

        // --- create page version ---
        var pageVersion = {
            id : idGenerator,
            taskcode:"",
            taskInst:"",
            srcFileUrl : "",
            srcOrgUrl : "",
            verTime : ""
        }




        for (var i in prodInfo ) {
            console.log(i + " , " + prodInfo[i]);
        }







        //client.close();

    });



};


