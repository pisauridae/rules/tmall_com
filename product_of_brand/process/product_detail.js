var DomeHelper = require("domhelper.js").DomeHelper;

/**
 * parse page in handle
 * @param page
 * @param spliderCtx
 * @constructor
 */
function MainProcess(page) {


    // --- get document ----
    doc = DomHelper.createDomcument(page.getDoc());

    var productInfo = {};
    doc.Find("div.tb-detail-hd h1").Each(function(i , sel) {
        try {
            var title = sel.Text().trim();
            productInfo.title = title
        } catch (e) {
            console.log("execption : " + e)
        }
    });
    // --- get brand ---
    doc.Find("b.J_EbrandLogo").Each(function(i , sel) {
        try {
            var brand = sel.Text().trim();
            productInfo.brand = brand
        } catch (e) {
            console.log("execption : " + e)
        }
    });
    doc.Find("a.shopLink").Each(function(i , sel) {
        try {
            var shopName = sel.Text().trim();
            productInfo.shopName = shopName
        } catch (e) {
            console.log("execption : " + e)
        }
    });


    var productCate = {};
    doc.Find("div.tb-detail-hd p.newp").Each(function(i , sel) {
        try {
            var cate = sel.Text().trim();
            productCate.cate = cate
        } catch (e) {
            console.log("execption : " + e)
        }
    });

    var productAttr = {}
    doc.Find("ul#J_AttrUL li").Each(function(i , sel) {
        // --- product link ---

        try {
            var attrWithName = sel.Text().trim();
            var attrArr = attrWithName.split(": ");
            productAttr[attrArr[0]] = attrArr[1]

        } catch (e) {
            console.log("execption : " + e)
        }
    });

    // ------------- product good status -------

    var prodGoodStatus = {}
    doc.Find("span#J_CollectCount").Each(function(i , sel) {
        // --- product link ---
        try {
            var attr = sel.Text().trim();
            prodGoodStatus.collect = attr
        } catch (e) {
            console.log("execption : " + e)
        }
    });

    doc.Find("div.tm-indcon").Each(function(i , sel) {
        // --- product link ---
        var label = "";
        var value = "";

        sel.Find("span.tm-label").Each(function(i , node) {
            label = node.Text().trim();
        });

        if (label) {
            sel.Find("span.tm-count").Each(function(i , node) {
                value = node.Text().trim();
                prodGoodStatus[label] = value;

            });
        }
    });

    var prodPrices = {}
    doc.Find("div.tm-fcs-panel dl").Each(function(i , sel) {
        // --- product link ---
        var label = "";
        var value = "";

        sel.Find("dt.tb-metatit").Each(function(i , node) {
            label = node.Text().trim();
        });

        if (label) {
            sel.Find("span.tm-price").Each(function(i , node) {
                value = node.Text().trim();
                prodPrices[label] = value;
            });
        }
    });


    var productImgs = [];
    doc.Find("ul#J_UlThumb img").Each(function(i , sel) {
        // --- product link ---
        var label = "";
        var value = "";

        var tagSrc = DomHelper.getAttrValue(sel,"src").trim();
        extLoc = tagSrc.indexOf(".jpg");
        var imgPath = tagSrc.substring(0, extLoc+4);

        productImgs.push( "https:"+imgPath );

    });


    var productExtImg = [];
    doc.Find("div#description img").Each(function(i , sel) {
        // --- product link ---
        var label = "";
        var value = "";

        var tagSrc = DomHelper.getAttrValue(sel,"data-ks-lazyload");

        productExtImg.push( tagSrc.trim() );

    });

    /*

    */
    // --- add the value to item ---
    try {
        // ---- check object ----
        page.addResultItem("productInfo", productInfo);
        page.addResultItem("productCate", productCate);
        page.addResultItem("productAttr", productAttr);
        page.addResultItem("productGoodStatus", prodGoodStatus);
        page.addResultItem("prodPrices", prodPrices);
        page.addResultItem("productImgs", productImgs);

    } catch (e) {

        console.log(e)

    }




};


