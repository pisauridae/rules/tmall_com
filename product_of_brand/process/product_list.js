/**
 * parse page in handle
 * @param page
 * @param spliderCtx
 * @constructor
 */
function MainProcess(page) {


    doc = DomHelper.createDomcument(page.getDoc());

    var selection = doc.Find("div.product-iWrap");

    var prodList = [];

    // ---- list products in page  ---
    for (var i = 0 ; i < selection.Length() ;i++) {
        var productInfo = {}

        wrapperObj = selection.Get(i)

        tagDivImg = selection.Find("div.productImg-wrap")

        tagDivImg.Find("a").Each(function(i , sel) {
            // --- product link ---
            try {
                attr = DomHelper.getAttrValue(sel,"href")
                productInfo.link = attr
            } catch (e) {
                console.log()
            }

        });




        // get the price

        selection.Find("p.productPrice em").Each(function(i , sel) {
            // --- product link ---
            //attr = DomHelper.GetAttrValue(sel,"title")
            try {
                productInfo.price = sel.Text()
            } catch (e) {
                console.log(e)
            }

        });

        // get product status
        selection.Find("p.productStatus span em").Each(function(i , sel) {
            // --- product link ---
            //attr = DomHelper.GetAttrValue(sel,"title")
            try {
                productInfo.transactions = sel.Text()
            } catch (e) {
                console.log(e)
            }

        });

        // --- add array ---
        prodList.push(productInfo);

    }

    // ---- parse page url ----
    var selection = doc.Find("p.ui-page-s");
    selection.Each(function(i , sel) {
        // --- product link ---

        tagPageSize = sel.Find("b.ui-page-s-len");
        try {
            var pageSize = tagPageSize.Text()
        } catch (e) {
            console.log(e)
        }
    })

    // ---- add product link to url ---
    prodList.forEach(function(prodInfo) {

        try {

            SpliderContext.addRequest( prodInfo.link );

        } catch (e) {

            console.log(e);
        }

    });

    // --- append the the base url and location ---
    var reqs = [];

    try {

        var currentPage = page.getPageUrl();
        var url = UrlHelper.parse( currentPage );

        var prefix = url.Scheme + "://" + url.Host + url.Path;
        var allPages = doc.Find("b.ui-page-num a");
        allPages.Each(function(i , sel) {
            // --- product link ---

            // --- add the link ----
            try {
                attr = DomHelper.getAttrValue(sel,"href");
                var reqUrl = prefix + attr.trim();
                var index = reqs.indexOf(reqUrl);
                if (index == -1) {
                    reqs.push( reqUrl );
                }
            } catch (e) {
                console.log()
            }
        });


        reqs.forEach(function(pageLink) {

            try {

                SpliderContext.addRequest( pageLink );

            } catch (e) {

                console.log(e);
            }

        });


    } catch (e) {

        console.log(e);
    }






}
